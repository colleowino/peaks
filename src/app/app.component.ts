import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  public searchField!: FormControl;
  private subject: Subject<any> = new Subject();

  constructor(private route: Router) {}

  ngOnInit() {
    this.searchField = new FormControl('');
    this.searchField.valueChanges
      .pipe(debounceTime(400))
      .subscribe((searchText) => {
        this.route.navigate(['/search'], { queryParams: { q: searchText } });
      });
  }
}
