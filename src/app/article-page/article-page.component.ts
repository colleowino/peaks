import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Article } from '../models/article';
import { GuardianAPIService } from '../services/guardian-api.service';
import { BookmarkService } from '../services/bookmark.service';
import { ThrowStmt } from '@angular/compiler';

@Component({
  selector: 'app-article-page',
  templateUrl: './article-page.component.html',
  styleUrls: ['./article-page.component.scss'],
})
export class ArticlePageComponent implements OnInit {
  private id: string = '';
  public newsArticle!: Article;
  public body: string[] = [];
  public bookmarkSaved: boolean = false;
  public buttonConfig = {};
  public contentLoaded = false;
  public error: boolean = false;
  public snackBarCss = 'snack-bar';

  constructor(
    private route: ActivatedRoute,
    private api: GuardianAPIService,
    private bookmaker: BookmarkService
  ) {}

  ngOnInit(): void {
    this.route.queryParams.subscribe((params) => {
      this.id = params['id'];
      this.loadArticle(this.id);
    });
  }

  loadArticle(id: string): void {
    this.api.fetchArticle(id).subscribe(
      (data) => {
        this.newsArticle = data;
        this.bookmarkSaved = this.bookmaker.isSaved(this.newsArticle);
      },
      (err) => (this.error = err),
      () => (this.contentLoaded = true)
    );
  }

  saveBookmark() {
    this.bookmaker.save(this.newsArticle!);
    this.bookmarkSaved = true;
    this.snackBarCss = 'snack-bar snack-bar_success';
    this.removePopUp();
  }

  removeBookmark() {
    this.bookmaker.delete(this.newsArticle!);
    this.bookmarkSaved = false;
    this.snackBarCss = 'snack-bar snack-bar_danger';
    this.removePopUp();
  }

  removePopUp() {
    setTimeout(() => {
      this.snackBarCss = 'snack-bar';
    }, 3000);
  }
}
