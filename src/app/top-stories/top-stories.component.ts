import { Component, OnInit } from '@angular/core';
import { Article } from '../models/article';
import { GuardianAPIService } from '../services/guardian-api.service';
import { OnDropdownChange } from '../interfaces/on-dropdown-change';
import { DateSortingService } from '../services/date-sorting.service';
import { DateOrder } from '../enums/date-order.enum';

@Component({
  selector: 'app-top-stories',
  templateUrl: './top-stories.component.html',
  styleUrls: ['./top-stories.component.scss'],
})
export class TopStoriesComponent implements OnInit {
  constructor(
    private ds: DateSortingService,
    private api: GuardianAPIService
  ) {}

  public pageTitle: string = 'Top stories';
  public newsStories: Article[] = [];
  public sportsStories: Article[] = [];
  public storiesLoaded: boolean = false;
  private error: boolean = false;

  ngOnInit(): void {
    this.fetchNews();
  }

  fetchNews() {
    this.api.fetchSection('news').subscribe(
      (value) => {
        this.newsStories.push(value);
      },
      (err) => (this.error = err),
      () => {
        this.newsStories = this.ds.sortArticlesByDate(
          this.newsStories,
          DateOrder.Newest
        );
        this.fetchSports();
      }
    );
  }

  fetchSports() {
    this.api.fetchSection('sport').subscribe(
      (value) => {
        this.sportsStories.push(value);
      },
      (err) => (this.error = err),
      () => {
        this.sportsStories = this.ds.sortArticlesByDate(
          this.sportsStories,
          DateOrder.Newest
        );
        this.storiesLoaded = true;
      }
    );
  }

  orderResultsByDate(sortingOrder: string): void {
    let sort = sortingOrder == 'newest' ? DateOrder.Newest : DateOrder.Oldest;
    this.newsStories = this.ds.sortArticlesByDate(this.newsStories, sort);
    this.sportsStories = this.ds.sortArticlesByDate(this.sportsStories, sort);
  }
}
