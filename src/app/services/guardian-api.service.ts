import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Article } from '../models/article';
import { mergeMap, map } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import * as cheerio from 'cheerio';

@Injectable({
  providedIn: 'root',
})
export class GuardianAPIService {
  constructor(private http: HttpClient) {}
  private guardianOpUrl = 'https://content.guardianapis.com';
  private params = new HttpParams()
    .set('show-fields', 'all')
    .set('page-size', '15')
    .set('show-elements', 'all')
    .set('api-key', environment.apiKey);

  /**
   * maps json to Article object
   * @param {string} raw json input
   * @return {Article}
   */

  private mapToArticle(json: any): Article {
    let $ = cheerio.load(json.fields.main || '');
    let thumbnail = $('img').attr('src') || json.fields.thumbnail;
    let thumbnailAlt = $('img').attr('alt');
    let thumbnailCaption = $('.element-image__caption').text();
    let thumbnailCredit = $('.element-image__credit').text();
    $ = cheerio.load(json.fields.standfirst || '');
    let subtitle = $('p').text();

    return new Article(
      json.id,
      json.sectionName,
      json.webTitle,
      subtitle,
      thumbnail,
      thumbnailAlt,
      thumbnailCaption,
      thumbnailCredit,
      json.fields.body,
      json.fields.bodyText,
      json.webPublicationDate
    );
  }

  private newsUrl: string = 'assets/data.json';
  /**
   * Return an observable to the top news
   * @return {observable<Article>}
   */

  public fetchTopStories(): Observable<Article> {
    return this.http.get(this.newsUrl).pipe(
      mergeMap((res: any) => res.response.results),
      map(this.mapToArticle)
    );
  }

  /**
   * Return an observable with a single news item
   * @param id 'string' article-id
   * @return {observable<Article>}
   */
  public fetchArticle(id: string): Observable<Article> {
    let url = `${this.guardianOpUrl}/${id}`;
    return this.http
      .get(url, { params: this.params })
      .pipe(map((data: any) => this.mapToArticle(data.response.content)));
  }

  /**
   * Return an observable with content from a single section
   * @return {observable<Article>}
   */
  public fetchSection(section: string): Observable<Article> {
    let url = `${this.guardianOpUrl}/${section}`;
    return this.http.get(url, { params: this.params }).pipe(
      mergeMap((res: any) => res.response.results),
      map(this.mapToArticle)
    );
  }

  /**
   * Return an observable with content from a single section
   * @return {observable<Article>}
   */
  public fetchSearch(
    searchTerm: string,
    pageNum: string = '1'
  ): Observable<Article> {
    let pageParam = this.params.set('page', pageNum);
    let url = `${this.guardianOpUrl}/search?q=${searchTerm}`;
    return this.http.get(url, { params: pageParam }).pipe(
      mergeMap((res: any) => res.response.results),
      map(this.mapToArticle)
    );
  }
}
