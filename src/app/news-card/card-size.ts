export type NewsCardSize =
  | 'tiny'
  | 'small'
  | 'medium'
  | 'medium-with-subtitle'
  | 'large';
