import { Component, OnInit } from '@angular/core';
import { DateOrder } from '../enums/date-order.enum';
import { OnDropdownChange } from '../interfaces/on-dropdown-change';
import { Article } from '../models/article';
import { BookmarkService } from '../services/bookmark.service';
import { DateSortingService } from '../services/date-sorting.service';

@Component({
  selector: 'app-bookmark-page',
  templateUrl: './bookmark-page.component.html',
  styleUrls: ['./bookmark-page.component.scss'],
})
export class BookmarkPageComponent implements OnInit, OnDropdownChange {
  public bookmarks!: Article[];
  public pageTitle = 'All bookmarks';
  constructor(
    private bookmarker: BookmarkService,
    private ds: DateSortingService
  ) {}

  ngOnInit(): void {
    this.bookmarks = this.ds.sortArticlesByDate(
      this.bookmarker.getBookMarks(),
      DateOrder.Newest
    );
  }

  orderResultsByDate(sortingOrder: string): void {
    let sort = sortingOrder == 'newest' ? DateOrder.Newest : DateOrder.Oldest;
    this.bookmarks = this.ds.sortArticlesByDate(this.bookmarks, sort);
  }
}
