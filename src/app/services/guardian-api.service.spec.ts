import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { GuardianAPIService } from './guardian-api.service';

describe('GuardianAPIService', () => {
  let service: GuardianAPIService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    service = TestBed.inject(GuardianAPIService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
