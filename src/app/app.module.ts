import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NewsCardComponent } from './news-card/news-card.component';
import { HttpClientModule } from '@angular/common/http';
import { TopStoriesComponent } from './top-stories/top-stories.component';
import { ArticlePageComponent } from './article-page/article-page.component';
import { PageHeaderComponent } from './page-header/page-header.component';
import { BookmarkPageComponent } from './bookmark-page/bookmark-page.component';
import { LoadingScreenComponent } from './loading-screen/loading-screen.component';
import { SearchPageComponent } from './search-page/search-page.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    NewsCardComponent,
    TopStoriesComponent,
    ArticlePageComponent,
    PageHeaderComponent,
    BookmarkPageComponent,
    LoadingScreenComponent,
    SearchPageComponent,
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
