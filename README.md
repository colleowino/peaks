# ThePeaks

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.2.0.
This project uses The Guardian's open platform api to display and search news stories.

A live version is available on [surge](http://aboard-doctor.surge.sh/)

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Deployment:

You will require to add a key in the environment.ts file `apiKey`

## Running unit tests

Run `npm run test` to execute the unit tests via [Jest](https://jestjs.io/).
