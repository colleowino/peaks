import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

/**
 * This is a container for the date sorting dropdown
 * parent components to implement 'OnDropdownChange' to update articles
 * @input title 'string' page title
 * @input dropdownChange 'event handler' parent provides listener for sorting
 */

@Component({
  selector: 'app-page-header',
  templateUrl: './page-header.component.html',
  styleUrls: ['./page-header.component.scss'],
})
export class PageHeaderComponent implements OnInit {
  constructor() {}

  @Input() title: string | undefined;
  @Output() dropdownChange = new EventEmitter<string>();

  ngOnInit(): void {}

  changeOrder(e: any) {
    this.dropdownChange.emit(e.target.value);
  }
}
