export interface OnDropdownChange {
  orderResultsByDate(sortOrder: string): void;
}
