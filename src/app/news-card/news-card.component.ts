import { Component, OnInit, Input } from '@angular/core';
import { Article } from '../models/article';
import { NewsCardSize } from './card-size';

@Component({
  selector: 'app-news-card',
  templateUrl: './news-card.component.html',
  styleUrls: ['./news-card.component.scss'],
})
export class NewsCardComponent implements OnInit {
  @Input() size: NewsCardSize = 'medium';
  @Input() article: Article | undefined;

  imagePlaceHolder = 'assets/Logo-white.png';
  displayClass = 'news news_large news_politics';

  constructor() {}

  ngOnInit(): void {
    this.displayClass = `news news_politics news_${this.size}`;
  }
}
