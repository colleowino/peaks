import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BookmarkPageComponent } from './bookmark-page/bookmark-page.component';
import { ArticlePageComponent } from './article-page/article-page.component';
import { SearchPageComponent } from './search-page/search-page.component';
import { TopStoriesComponent } from './top-stories/top-stories.component';

const routes: Routes = [
  { path: 'article', component: ArticlePageComponent },
  { path: 'top-stories', component: TopStoriesComponent },
  { path: 'bookmarks', component: BookmarkPageComponent },
  { path: 'search', component: SearchPageComponent },
  { path: '', redirectTo: '/top-stories', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
