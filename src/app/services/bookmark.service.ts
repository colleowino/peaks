import { Attribute, Injectable } from '@angular/core';
import { environment as env } from '../../environments/environment';
import { Article } from '../models/article';

@Injectable({
  providedIn: 'root',
})
export class BookmarkService {
  constructor() {}

  public getBookMarks(): Article[] {
    let json = localStorage.getItem(env.bookmarkKey) || '';
    return json == '' ? [] : JSON.parse(json);
  }

  public isSaved(article: Article) {
    let storedBookmarks = this.getBookMarks();
    let match = storedBookmarks.filter((item) => article.id == item.id);

    return match.length > 0;
  }

  public persistChanges(bookmarks: Article[]) {
    let json = JSON.stringify([...bookmarks]);
    localStorage.setItem(env.bookmarkKey, json);
  }

  public save(article: Article) {
    if (!this.isSaved(article)) {
      this.persistChanges(this.getBookMarks().concat(article!));
    }
  }

  public delete(article: Article) {
    let nonMatching = this.getBookMarks().filter(
      (item) => article.id != item.id
    );
    this.persistChanges(nonMatching);
  }
}
