import { Injectable } from '@angular/core';
import { DateOrder } from '../enums/date-order.enum';
import { Article } from '../models/article';

@Injectable({
  providedIn: 'root',
})
export class DateSortingService {
  constructor() {}

  public sortArticlesByDate(
    articles: Article[],
    sortOrder: DateOrder
  ): Article[] {
    return articles.sort((a, b) => {
      let x = Number(new Date(a.publicationDate).valueOf());
      let y = Number(new Date(b.publicationDate).valueOf());

      if (sortOrder == DateOrder.Oldest) {
        return x - y;
      } else {
        return y - x;
      }
    });
  }
}
