export class Article {
  public constructor(
    public id: string,
    public sectionName: string,
    public webTitle: string,
    public subtitle: string,
    public thumbnail: string | undefined,
    public thumbnailAlt: string | undefined,
    public thumbnailCaption: string,
    public thumbnailCredit: string,
    public body: string,
    public bodyText: string,
    public publicationDate: Date
  ) {}
}
