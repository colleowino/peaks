import { Component, HostListener, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DateOrder } from '../enums/date-order.enum';
import { Article } from '../models/article';
import { DateSortingService } from '../services/date-sorting.service';
import { GuardianAPIService } from '../services/guardian-api.service';

@Component({
  selector: 'app-search-page',
  templateUrl: './search-page.component.html',
  styleUrls: ['./search-page.component.scss'],
})
export class SearchPageComponent implements OnInit {
  // private searchTerm: string = '';

  public searchResults: Article[] = [];
  public pageTitle: string = 'search result';
  public contentLoaded: boolean = false;
  public searchTerm: string = '';
  private currentPage: string = '1';
  private error: boolean = false;
  public loadingMore: boolean = false;

  constructor(
    private route: ActivatedRoute,
    private api: GuardianAPIService,
    private ds: DateSortingService
  ) {}
  atTheEnd(): boolean {
    return true;
  }
  ngOnInit(): void {
    window.addEventListener('scroll', this.onScroll.bind(this), true);
    this.route.queryParams.subscribe((params) => {
      let searchTerm = params['q'] || '';
      this.searchTerm = searchTerm;
      if (searchTerm.length > 0) {
        this.clearPastSearch(); // incase a new search begins on the search page
        this.loadSearch();
      }
    });
  }

  ngOnDestroy() {
    window.removeEventListener('scroll', this.onScroll, true);
  }

  clearPastSearch() {
    this.searchResults = [];
    this.contentLoaded = false;
    this.currentPage = '1';
  }

  loadSearch() {
    this.api.fetchSearch(this.searchTerm, this.currentPage).subscribe(
      (value) => {
        this.searchResults.push(value);
      },
      (err) => (this.error = err),
      () => {
        this.searchResults = this.ds.sortArticlesByDate(
          this.searchResults,
          DateOrder.Newest
        );
        this.contentLoaded = true;
        // this.loadingMore = false; // always reset the infinite load
      }
    );
  }

  async onScroll($event: any): Promise<void> {
    // console.log($event);
    let pos =
      (document.documentElement.scrollTop || document.body.scrollTop) +
      document.documentElement.offsetHeight;
    let max: number = document.documentElement.scrollHeight;

    // infinite scroll
    if (pos == max) {
      this.currentPage = (Number(this.currentPage) + 1).toString();
      this.loadingMore = true;

      this.api.fetchSearch(this.searchTerm, this.currentPage).subscribe(
        (value) => {
          this.searchResults.push(value);
        },
        (err) => (this.error = err),
        () => {
          this.searchResults = this.ds.sortArticlesByDate(
            this.searchResults,
            DateOrder.Newest
          );
          this.loadingMore = false;
        }
      );
    }
  }

  orderResultsByDate(sortingOrder: string): void {
    let sort = sortingOrder == 'newest' ? DateOrder.Newest : DateOrder.Oldest;
    this.searchResults = this.ds.sortArticlesByDate(this.searchResults, sort);
  }
}
